package main

import (
        "flag"
        "fmt"
        "os"
        "strings"

        "gitlab.com/go-cmds/goextractor/pkg/extractor"
)




func main() {

        var ver = "0.1"
        var version bool
        flag.BoolVar(&version, "version", false, "version output")
        flag.Parse()
        if version {
            fmt.Println(os.Args[0] + " version is " + ver)
            os.Exit(0)
        }


        //fmt.Println("len", len(os.Args))

        if len(os.Args) < 2 {
        fmt.Println("Usage:", os.Args[0], "<FILE TO EXTRACT>")
        return
        }

        //for _, arg := range os.Args[1:] {
        //      fmt.Println(arg)
        //}

        fileName := os.Args[1]
        ss := strings.Split(fileName, ".")
        ext := ss[len(ss)-1]
        //fmt.Println(ext)

        if ext == "gz" || ext == "bz2" || ext == "lzma" {
         ext2 := ss[len(ss)-2]
         ext = ext2+"."+ext
        }

        if fileName == ext {
          fmt.Println("Please pass a file with an extension")
          return
        }

        fmt.Println("Extracting:",fileName,"with an extension of",ext)


        if ext == "zip" {
          fmt.Println("Extracting Zip File")

          	zip := archive_extractor.New(archive_extractor.Zip)
            headers, err := zip.Extract(fileName)
            if err != nil {
              fmt.Print(err.Error())
            }
            fmt.Print(headers)

        }


}
